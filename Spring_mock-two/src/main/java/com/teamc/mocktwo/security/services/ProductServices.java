package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.ProductDto;
import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.exception.MyException;
import com.teamc.mocktwo.models.Product;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServices {
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    
    public List<ProductDto> listProducts(){
    
        List<Product> list = productRepository.findAll();
        List<ProductDto> listDTO = new ArrayList<>();
        for (Product l:list) {
            listDTO.add(modelMaper.map(l, ProductDto.class));
        }
        return listDTO;
    }
    public List<ProductDto> addProduct(Product product){
        if(!productRepository.existsByName(product.getName())){
            productRepository.save(product);
        }
        List<Product> list = productRepository.findAll();
        List<ProductDto> listDTO = new ArrayList<>();
        for (Product l:list) {
            listDTO.add(modelMaper.map(l, ProductDto.class));
        }
        return listDTO;
    }
    public List<ProductDto> updateProduct(Product product){
        if(!productRepository.existsByName(product.getName())) {
            Product p = productRepository.findById(product.getId()).get();
            p = product;
            productRepository.save(p);
        }else{
            System.out.println("Không tìm thấy sản phẩm cần sửa!");
        }
        List<Product> list = productRepository.findAll();
        List<ProductDto> listDTO = new ArrayList<>();
        for (Product l:list) {
            listDTO.add(modelMaper.map(l, ProductDto.class));
        }
        return listDTO;
    }
    public List<ProductDto> changeQuantityProduct(Product product, Long number) {
        Product p = productRepository.findById(product.getId()).get();
        if(p.getQuantity() >= number){
            p.setQuantity(p.getQuantity()-number);
        }else{
            System.out.println("Số sản phẩm còn lại ít hơn số cần giảm!");
        }
        productRepository.save(p);
        List<Product> list = productRepository.findAll();
        List<ProductDto> listDTO = new ArrayList<>();
        for (Product l:list) {
            listDTO.add(modelMaper.map(l, ProductDto.class));
        }
        return listDTO;
    }
    public List<ProductDto> deleteProduct(Product product){
        if(productRepository.existsByName(product.getName())){
            productRepository.deleteById(product.getId());
        }
        List<Product> list = productRepository.findAll();
        List<ProductDto> listDTO = new ArrayList<>();
        for (Product l:list) {
            listDTO.add(modelMaper.map(l, ProductDto.class));
        }
        return listDTO;
    }
}

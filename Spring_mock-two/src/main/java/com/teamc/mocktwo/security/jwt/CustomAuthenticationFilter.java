package com.teamc.mocktwo.security.jwt;

import com.teamc.mocktwo.PropertyLoader;
import com.teamc.mocktwo.security.services.UserDetailsImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.stream.Collectors;

@Slf4j
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    //Load application.properties
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);
    private String jwtSecret;
    private int jwtExpirationMs;
    //Tham khảo tại UsernamePasswordAuthenticationFilter, cấu hình filter này chỉ hoạt động ở request kiểu POST
    private boolean postOnly = true;
    private final AuthenticationManager authenticationManager;
    
    public CustomAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
    
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (this.postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println("Đang xác thực người dùng!!!!!!!!!!");
        log.info("Username is {}; Password is {}", username, password);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        //Tại đây vì đang sử dụng JWT nên sẽ ko cần setDetail cho request nữa (setDetail - google it)
        return authenticationManager.authenticate(authenticationToken);
    }
    
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
        System.out.println("Xác thực người dùng thành công!!!!!!!!!");
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        //Load application.properties
        try {
            jwtSecret = PropertyLoader.loadProperties().getProperty("jwtSecret");
            jwtExpirationMs = Integer.parseInt(PropertyLoader.loadProperties().getProperty("jwtExpirationMs"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String access_token = Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date())
                .claim("roles", userDetails.getAuthorities().stream().map(SimpleGrantedAuthority::getAuthority).collect(Collectors.joining(", ")))
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
        String refesh_token = Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date())
                .claim("roles", userDetails.getAuthorities().stream().map(SimpleGrantedAuthority::getAuthority).collect(Collectors.joining(", ")))
                .setExpiration(new Date((new Date()).getTime() + 24*jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
        response.setHeader("access_token", access_token);
        response.setHeader("refresh_token", refesh_token);
    }
    
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);
    }
}

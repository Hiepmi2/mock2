package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.models.*;
import com.teamc.mocktwo.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class OrderServices {
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private CartRepository cartRepository;
    
    @Autowired
    private Cart_ItemRepository cartItemRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserServices userServices;
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private Order_ItemRepository orderItemRepository;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public List<UserDto> addOrder() {
        User user = userServices.getUser();
        Order order = new Order(user);
        orderRepository.save(order);
        user.addOrder(order);
        Iterable<Cart_Item> listCartItem = cartItemRepository.findByEnable();
        if (listCartItem != null){
            for (Cart_Item ci : listCartItem) {
                Order_Item orderItem = new Order_Item(order, ci.getProduct(), ci.getQuantity_product());
                orderItemRepository.save(orderItem);
                order.addOrderItem(orderItem);
                order.setAmount_total(order.getAmount_total()+orderItem.getQuantity_product()*orderItem.getProduct().getPrice());
            }
            for(Cart_Item ci : user.getCart().getCart_Item()){
                if(ci.getEnable()==true){
                    user.getCart().getCart_Item().remove(ci);
                }
            }
            user.getCart().setPrice_item_enable(Long.valueOf(0));
            userRepository.save(user);
        }
        return userServices.listUser();
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public List<UserDto> deleteOrder(Order order) {
        User user = userServices.getUser();
        orderItemRepository.deleteByOrder_id(order.getOrder_id());
        orderRepository.deleteByOrder_id(order.getOrder_id());
        Iterable<Product> list1 = productRepository.findAll();
        return userServices.listUser();
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public List<UserDto> rollBackOrder(Order order) {
        User user = userServices.getUser();
        orderRepository.save(order);
        user.addOrder(order);
        Iterable<Cart_Item> listCartItem = cartItemRepository.findByEnable();
        if (listCartItem != null){
            for (Cart_Item ci : listCartItem) {
                Order_Item orderItem = new Order_Item(order, ci.getProduct(), ci.getQuantity_product());
                orderItemRepository.save(orderItem);
                order.addOrderItem(orderItem);
                order.setAmount_total(order.getAmount_total()+orderItem.getQuantity_product()*orderItem.getProduct().getPrice());
            }
        }
        return userServices.listUser();
    }
    
}
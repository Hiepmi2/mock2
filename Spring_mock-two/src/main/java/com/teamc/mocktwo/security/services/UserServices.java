package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.models.Cart;
import com.teamc.mocktwo.models.Role;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.CartRepository;
import com.teamc.mocktwo.repository.RoleRepository;
import com.teamc.mocktwo.repository.UserRepository;
import com.teamc.mocktwo.security.jwt.JwtUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServices {
    @Autowired
    AuthenticationManager authenticationManager;
    
    @Autowired
    PasswordEncoder encoder;
    
    @Autowired
    JwtUtils jwtUtils;
    
    @Autowired
    @Qualifier("UserRepository")
    UserRepository userRepository;
    
    @Autowired
    @Qualifier("CartRepository")
    CartRepository cartRepository;
    
    @Autowired
    @Qualifier("RoleRepository")
    RoleRepository roleRepository;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public UserDto addUserRole(User user) {
        User u = userRepository.findByUsername(user.getUsername()).get();
        List<Role> listRole = user.getRoles();
        u.setRoles(listRole);
        userRepository.save(u);
        return modelMaper.map(userRepository.findByUsername(user.getUsername()).get(), UserDto.class);
    }
    public List<UserDto> listUser() {
        List<User> list = userRepository.findAll();
        List<UserDto> listDTO = new ArrayList<UserDto>();
        for (User u:list) {
            listDTO.add(modelMaper.map(u, UserDto.class));
        }
        return listDTO;
    }
    public List<UserDto> enableUser(User user) {
        user = userRepository.findByUsername(user.getUsername()).get();
        user.setStatus(true);
        userRepository.save(user);
        return listUser();
    }
    public List<UserDto> disableUser(User user) {
        user = userRepository.findByUsername(user.getUsername()).get();
        user.setStatus(false);
        userRepository.save(user);
        return listUser();
    }
    public List<UserDto> addUser(User user) {
        List<UserDto> listDTO = signUpUser(user);
        return listDTO;
    }
    public List<UserDto> signUpUser(User user) {
        if(!userRepository.existsByUsername(user.getUsername())){
            String passwordEncode = encoder.encode(user.getPassword());
            user.setPassword(passwordEncode);
            userRepository.save(user);
            user = userRepository.findByUsername(user.getUsername()).get();
            Cart cart  = new Cart();
            cart.setUser(user);
            user.setCart(cart);
            cartRepository.save(cart);
            userRepository.save(user);
        }else{
            System.out.println("Người dùng tồn tại!");
        }
        return listUser();
    }
    public List<UserDto> deleteUser(User user) {
        Long id = user.getId();
        if(userRepository.existsById(user.getId())){
            userRepository.deleteById(id);
        }else{
            System.out.println("Người cần xóa không tồn tại!");
        }
        return listUser();
    }
    public User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        // Tìm lên người dùng và sản phẩm cần thao tac
        User user = userRepository.findByUsername(username).get();
        System.out.println("Người dùng thực hiện thao tác này là: " + user.getUsername());
        return user;
    }
    
}

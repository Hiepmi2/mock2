package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.ProductDto;
import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.models.Cart;
import com.teamc.mocktwo.models.Cart_Item;
import com.teamc.mocktwo.models.Product;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.CartRepository;
import com.teamc.mocktwo.repository.Cart_ItemRepository;
import com.teamc.mocktwo.repository.ProductRepository;
import com.teamc.mocktwo.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class CartServices {
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private CartRepository cartRepository;
    
    @Autowired
    private Cart_ItemRepository cartItemRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserServices userServices;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    
    public List<UserDto> addToCart(Product product){
        User user = userServices.getUser();
        Product product1 = productRepository.findById(product.getId()).get();
        Cart_Item cart_Item = cartItemRepository.findByProduct_id(product1.getId());
        Cart cart = user.getCart();
        Cart_Item cartItem = null;
        // Kiểm tra tồn tại cart Item
        if(cart_Item == null){
            //nếu sản phẩm chưa tồn tại trong giỏ thi tạo sản phẩm và thêm vào, tính giá của giỏ và sản phẩm đó
           cartItem = new Cart_Item(user.getCart(), product1, product.getQuantity(), product1.getPrice()*product.getQuantity());
           cart.setPrice_total(cart.getPrice_total()+product1.getPrice()*product.getQuantity());
            product1.addCartItem(cartItem);
            user.getCart().addCartItem(cartItem);
        }else{
            cartItem = cart_Item;
            if(cart_Item.getQuantity_product()+ product.getQuantity()<= product1.getQuantity()){
                cartItem.setQuantity_product(cart_Item.getQuantity_product()+ product.getQuantity());
                cartItem.setPrice_total(cartItem.getPrice_total()+product1.getPrice()* product.getQuantity());
                cart.setPrice_total(cart.getPrice_total()+product1.getPrice()* product.getQuantity());
            }else{
                System.out.println("Số lượng không đủ!");
            }
            
        }
        cartItemRepository.save(cartItem);
        userRepository.save(user);
        productRepository.save(product1);
        
        List<User> list = userRepository.findAll();
        List<UserDto> listDTO = new ArrayList<UserDto>();
        for (User u:list) {
            listDTO.add(modelMaper.map(u, UserDto.class));
        }
        return listDTO;
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public List<UserDto> deleteFromCart(Product product){
        System.out.println("Kiểm tra người dùng xóa sản phẩm vào giỏ!");
        User user = userServices.getUser();
        Cart_Item cart_Item = cartItemRepository.findByProduct_id(product.getId());
        Cart cart = user.getCart();
        Product product1 = productRepository.findById(product.getId()).get();
        if(cart_Item==null){
            System.out.println("Không tìm thấy sản phẩm trong giỏ!");
        }else{
            if(product.getQuantity()>=cart_Item.getQuantity_product()){
                cartItemRepository.delete(cart_Item);
                disableProductInCart(product1);
                cart.setPrice_total(cart.getPrice_total()-cart_Item.getPrice_total());
            }else{
                //Không cần save lai vì có chú thích Transaction - tự động commit khi hết phương thức
                cart_Item.setQuantity_product(cart_Item.getQuantity_product()- product.getQuantity());
                cart_Item.setPrice_total(cart_Item.getPrice_total()-product1.getPrice()* product.getQuantity());
                cart.setPrice_total(cart.getPrice_total()-product1.getPrice()* product.getQuantity());
                if(cart_Item.getEnable()){
                    cart.setPrice_item_enable(cart.getPrice_item_enable()-product1.getPrice()* product.getQuantity());
                }
            }
        }
        List<User> list = userRepository.findAll();
        List<UserDto> listDTO = new ArrayList<UserDto>();
        for (User u:list) {
            listDTO.add(modelMaper.map(u, UserDto.class));
        }
        return listDTO;
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public List<UserDto> enableProductInCart(Product product){
        User user = userServices.getUser();
        Cart cart = user.getCart();
        Cart_Item cart_Item = cartItemRepository.findByProduct_id(product.getId());
        if(cart_Item==null){
            System.out.println("Không tìm thấy sản phẩm trong giỏ!");
        }else{
            cart_Item.setEnable(true);
            cart.setPrice_item_enable(cart.getPrice_item_enable()+cart_Item.getPrice_total());
        }
        List<User> list = userRepository.findAll();
        List<UserDto> listDTO = new ArrayList<UserDto>();
        for (User u:list) {
            listDTO.add(modelMaper.map(u, UserDto.class));
        }
        return listDTO;
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public List<UserDto> disableProductInCart(Product product){
        User user = userServices.getUser();
        Cart cart = user.getCart();
        Cart_Item cart_Item = cartItemRepository.findByProduct_id(product.getId());
        if(cart_Item==null){
            System.out.println("Không tìm thấy sản phẩm trong giỏ!");
        }else{
            cart_Item.setEnable(false);
            cart.setPrice_item_enable(cart.getPrice_item_enable()-cart_Item.getPrice_total());
        }
        List<User> list = userRepository.findAll();
        List<UserDto> listDTO = new ArrayList<UserDto>();
        for (User u:list) {
            listDTO.add(modelMaper.map(u, UserDto.class));
        }
        return listDTO;
    }
}
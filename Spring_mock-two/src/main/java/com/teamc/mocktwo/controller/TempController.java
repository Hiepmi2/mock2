package com.teamc.mocktwo.controller;

import com.teamc.mocktwo.dto.ProductDto;
import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.models.Order;
import com.teamc.mocktwo.models.Product;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.security.services.CartServices;
import com.teamc.mocktwo.security.services.OrderServices;
import com.teamc.mocktwo.security.services.ProductServices;
import com.teamc.mocktwo.security.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TempController {
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(){
        return "Helloworld";
    }
    @RequestMapping(value = "/api/admin", method = RequestMethod.GET)
    public String homeAdmin(){
        return "Hello Admin";
    }
    @RequestMapping(value = "/api/user", method = RequestMethod.GET)
    public String homeUser(){
        return "Hello User";
    }
    @RequestMapping(value = "/api/admin/test", method = RequestMethod.GET)
    public String homeAdminTest(){
        return "Hello Admin Test";
    }
    @RequestMapping(value = "/api/user/test", method = RequestMethod.GET)
    public String homeUserTest(){
        return "Hello User Test";
    }
    @RequestMapping(value = "/api/user/test/test", method = RequestMethod.GET)
    public String homeUserTest2(){
        return "Hello User Test1";
    }
    
    @Autowired
    private UserServices userServices;
    
    @Autowired
    private ProductServices productServices;
    
    @Autowired
    private CartServices cartServices;
    
    @Autowired
    private OrderServices orderServices;
    
    @RequestMapping(value = "/api/login", method = RequestMethod.GET)
    public String customLogin(){
        return "customLogin";
    }
    
    @PostMapping("/api/admin/add-user")
    public ResponseEntity<?> addUser(@Valid @RequestBody User user){
        List<UserDto> list = userServices.addUser(user);
        return ResponseEntity.ok(list);
    }
    @GetMapping("/api/admin/list-user")
    public ResponseEntity<?> listUser(){
        List<UserDto> list = userServices.listUser();
        return ResponseEntity.ok(list);
    }
    @PostMapping("/api/signup")
    public ResponseEntity<?> signUp(@Valid @RequestBody User user){
        List<UserDto> list = userServices.signUpUser(user);
        return ResponseEntity.ok(list);
    }
    
    @PostMapping("/api/admin/delete-user")
    public ResponseEntity<?> deteteUser(@Valid @RequestBody User user){
        List<UserDto> list = userServices.deleteUser(user);
        return ResponseEntity.ok(list);
    }
    
    @PostMapping("/api/admin/add-role-user")
    public ResponseEntity<?> addUserRole(@Valid @RequestBody User user){
        UserDto userDto = userServices.addUserRole(user);
        return ResponseEntity.ok(userDto);
    }
    @PostMapping("/api/list-product")
    public ResponseEntity<?> listProduct(){
        List<ProductDto> listDTO = productServices.listProducts();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/add-product")
    public ResponseEntity<?> addProduct(@Valid @RequestBody Product product){
        List<ProductDto> listDTO = productServices.addProduct(product);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/update-product")
    public ResponseEntity<?> updateProduct(@Valid @RequestBody Product product){
        List<ProductDto> listDTO = productServices.updateProduct(product);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/delete-product")
    public ResponseEntity<?> deleteProduct(@Valid @RequestBody Product product){
        List<ProductDto> listDTO = productServices.deleteProduct(product);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/add-to-cart")
    public ResponseEntity<?> addToCart(@Valid @RequestBody Product product){
        List<UserDto> listDTO = cartServices.addToCart(product);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/delete-from-cart")
    public ResponseEntity<?> deleteFromCart(@Valid @RequestBody Product product){
        List<UserDto> listDTO = cartServices.deleteFromCart(product);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/select-product-in-cart")
    public ResponseEntity<?> selectFromCart(@Valid @RequestBody Product product){
        // Nếu tích chọn (là lúc enable) không phải để mua mà là để xóa thì yêu cầu frontend trước khi gửi lệnh xóa thì gửi thêm lệnh disable
        List<UserDto> listDTO = cartServices.enableProductInCart(product);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/unselect-product-in-cart")
    public ResponseEntity<?> unselectFromCart(@Valid @RequestBody Product product){
        List<UserDto> listDTO = cartServices.disableProductInCart(product);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/add-order")
    public ResponseEntity<?> addOrder(){
        List<UserDto> listDTO = orderServices.addOrder();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/delete-order")
    public ResponseEntity<?> deleteOrder(@Valid @RequestBody Order order){
        List<UserDto> listDTO = orderServices.deleteOrder(order);
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/rollback-order")
    public ResponseEntity<?> rollBackOrder(@Valid @RequestBody Order order){
        List<UserDto> listDTO = orderServices.rollBackOrder(order);
        return ResponseEntity.ok(listDTO);
    }
    
    
}

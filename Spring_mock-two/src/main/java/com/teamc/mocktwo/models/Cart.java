package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "carts")
public class Cart implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "cart_id")
    @TableGenerator(name =  "cart_id", table = "_sequences", pkColumnValue = "cart_id", allocationSize = 1)
    @Column(name = "cart_id", nullable = false)
    private Long cart_id;
    
    @Column(name = "price_total", nullable = false)
    private Long price_total = Long.valueOf(0);
    
    
    @Column(name = "price_item_enable", nullable = false)
    private Long price_item_enable = Long.valueOf(0);
    
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", unique = true)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;
    
//    @ManyToOne(fetch = FetchType.LAZY, optional=false)
//    @JoinColumn(name = "user_id", nullable = false)
//    private User user;
    
    @OneToMany(mappedBy = "cart")
    private List<Cart_Item> cart_Item = new ArrayList<Cart_Item>();
    
    public Cart(Long cart_id) {
        this.cart_id = cart_id;
    }
    
    public void addCartItem(Cart_Item cart_item){
        this.cart_Item.add(cart_item);
    }
    public void deleteCartItem(Cart_Item cart_item){
        this.cart_Item.remove(cart_item.getCart_item_id());
    }
}
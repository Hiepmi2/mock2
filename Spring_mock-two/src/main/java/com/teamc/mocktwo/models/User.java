package com.teamc.mocktwo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "user_id")
    @TableGenerator(name =  "user_id", table = "_sequences", pkColumnValue = "user_id", allocationSize = 1)
    @Column(name = "user_id", nullable = false)
    private Long id;
    
    @Column(name = "username", nullable = false, length = 45, unique = true)
    private String username;
    
    @Column(name = "password", nullable = false, length = 120)
    private String password;
    
    @Column(name = "fullname", nullable = false, length = 45)
    private String fullname;
    
    @Column(name = "email", nullable = false, length = 45)
    private String email;
    
    @Column(name = "phone", nullable = false, length = 45)
    private String phone;
    
    @Column(name = "secret", nullable = false, length = 45)
    private String secret;
    
    @OneToMany(mappedBy = "user")
    private List<ProductReview> productReviews = new ArrayList<ProductReview>();
    
    @OneToMany(mappedBy = "user")
    private List<Order> orders = new ArrayList<>();
    
    @OneToMany(mappedBy = "user")
    private List<Transaction> transactions = new ArrayList<Transaction>();
    
    @OneToMany(mappedBy = "user")
    private List<Audit_Log> audit_logs = new ArrayList<Audit_Log>();
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles = new ArrayList<Role>();
    
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "user")
    private Cart cart ;
    
//    @OneToMany(mappedBy = "user")
//    private List<Cart> carts= new ArrayList<Cart>();;
    
    @Column(name = "status", nullable = false)
    private Boolean status;
    
    public User(Long id, String username, String password, String fullname, String email, String phone, String secret, Boolean status) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.secret = secret;
        this.status = status;
    }
    public User( String username, String password, String fullname, String email, String phone, String secret, Boolean status) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.secret = secret;
        this.status = status;
    }
    public void addRole(Role role){
        this.roles.add(role);
    }
    
    public void removeRole(Role role){
        this.roles.remove(role);
    }
    
    public void addOrder(Order order){
        this.orders.add(order);
    }
    
    public void removeOrder(Order order){
        this.orders.remove(order);
    }
    
    public void addProductReview(ProductReview productReview){
        this.productReviews.add(productReview);
    }
    
    public void removeProductReview(ProductReview productReview){
        this.productReviews.remove(productReview);
    }
    
    public void addTransaction(Transaction transaction){
        this.transactions.add(transaction);
    }
    
    public void removeTransaction(Transaction transaction){
        this.transactions.remove(transaction);
    }
    
    public void addAuditLog(Audit_Log audit_log){
        this.audit_logs.add(audit_log);
    }
    
    public void removeAuditLog(Audit_Log audit_log){
        this.audit_logs.remove(audit_log);
    }
    

}
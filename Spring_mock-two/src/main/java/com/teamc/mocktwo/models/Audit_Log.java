package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "audit_log")
public class Audit_Log implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "audit_log_id")
    @TableGenerator(name =  "audit_log_id", table = "_sequences", pkColumnValue = "audit_log_id", allocationSize = 1)
    @Column(name = "audit_log_id", nullable = false)
    private Long audit_log_id;
    
    @Column(name = "event", nullable = false, length = 200)
    private String event;
    
    @Column(name = "description", nullable = false, length = 1000)
    private String description;
    
    @Column(name = "tech_log", nullable = false)
    private Boolean isTechlog = false;
    
    
    @Column(name = "business_log", nullable = false)
    private Boolean isBusinesslog = false;
    
    @ManyToOne(fetch = FetchType.LAZY, optional=false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;

}
package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "orders")
public class Order implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "order_id")
    @TableGenerator(name =  "order_id", table = "_sequences", pkColumnValue = "order_id", allocationSize = 1)
    @Column(name = "order_id", nullable = false)
    private Long order_id;
    
    @ManyToOne(fetch = FetchType.LAZY, optional=false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;
    
    @OneToMany(mappedBy = "order")
    private List<Order_Item> order_items = new ArrayList<Order_Item>();
    
    @Column(name = "amount_total", nullable = false)
    private Long amount_total;
    
    @OneToMany(mappedBy = "order")
    private List<Transaction> transactions = new ArrayList<Transaction>();
    
    public Order(User user) {
        this.user = user;
        this.setAmount_total((long)0);
    }
    public void addOrderItem(Order_Item orderItem){
        this.order_items.add(orderItem);
    }
    public void removeOrderItem(Order_Item orderItem){
        this.order_items.remove(orderItem);
    }
}
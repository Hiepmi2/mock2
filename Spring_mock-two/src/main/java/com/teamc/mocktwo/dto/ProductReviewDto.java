package com.teamc.mocktwo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductReviewDto {
    private Long id;
    private String product_review;
}

package com.teamc.mocktwo.dto;

import com.teamc.mocktwo.models.Cart;
import com.teamc.mocktwo.models.Order;
import com.teamc.mocktwo.models.ProductReview;
import com.teamc.mocktwo.models.Role;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserDto {
    private Long id;
    private String username;
    private String fullname;
    private String email;
    private String phone;
    private List<Role> roles = new ArrayList<Role>();
//    private List<Cart> carts = new ArrayList<Cart>();
    private Cart cart;
    private Boolean status;
    private List<Order> orders = new ArrayList<>();
    //private List<ProductReview> productReviews = new ArrayList<ProductReview>();
    //private List<Order> orders = new ArrayList<>();
}

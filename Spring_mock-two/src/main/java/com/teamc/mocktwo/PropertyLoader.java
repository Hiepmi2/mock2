package com.teamc.mocktwo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyLoader {
    public static Properties loadProperties() throws IOException {
        Properties configuration = new Properties();
        InputStream inputStream = PropertyLoader.class
                .getClassLoader()
                .getResourceAsStream("application.properties");
        configuration.load(inputStream);
        inputStream.close();
        return configuration;
    }
}
package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.Cart_Item;
import com.teamc.mocktwo.models.Order_Item;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("Order_ItemRepository")
public interface Order_ItemRepository extends CrudRepository<Order_Item, Long> {
    @Modifying
    @Query(value = "delete from order_item b where b.order_id= ?1", nativeQuery = true)
    public void deleteByOrder_id(Long order_id);
}
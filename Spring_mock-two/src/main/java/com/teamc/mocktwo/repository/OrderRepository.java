package com.teamc.mocktwo.repository;


import com.teamc.mocktwo.models.Cart_Item;
import com.teamc.mocktwo.models.Order;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
@Qualifier("OrderRepository")
public interface OrderRepository extends CrudRepository<Order, Long> {
    @Modifying
    @Query(value = "delete from orders b where b.order_id= ?1", nativeQuery = true)
    public void deleteByOrder_id(Long order_id);
}
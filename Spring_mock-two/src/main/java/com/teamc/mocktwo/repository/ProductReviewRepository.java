package com.teamc.mocktwo.repository;


import com.teamc.mocktwo.models.ProductReview;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("ProductReviewRepository")
public interface ProductReviewRepository extends CrudRepository<ProductReview, Long> {
}
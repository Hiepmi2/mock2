package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.Transaction;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("TransactionRepository")
public interface TransactionRepository extends CrudRepository<Transaction, Long> {
}
package com.teamc.mocktwo.repository;


import com.teamc.mocktwo.models.Role;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Qualifier("RoleRepository")
public interface RoleRepository extends JpaRepository<Role, Long> {
    boolean existsByName(String name);
    
    Role findByName(String name);
}
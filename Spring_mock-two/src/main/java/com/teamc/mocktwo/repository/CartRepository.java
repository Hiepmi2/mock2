package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.Cart;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("CartRepository")
public interface CartRepository extends CrudRepository<Cart, Long> {
}